const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const lectureSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    course: {
      type: Schema.Types.ObjectId,
      ref: "Course",
    },
    week: {
      type: Number,
      required: true,
    },
    mentor_s: [
      {
        type: Schema.Types.ObjectId,
        ref: "Mentor",
      },
    ],
    duration: {
      type: Number,
      required: true,
    },
    students: [
      {
        type: Schema.Types.ObjectId,
        ref: "User",
      },
    ],
  },

  { timestamps: true }
);

module.exports = mongoose.model("Lecture", lectureSchema);
