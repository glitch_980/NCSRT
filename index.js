const express = require("express");
const cors = require("cors");
const session = require("express-session");
const passport = require("passport");
const passportLocalMongoose = require("passport-local-mongoose");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const findOrCreate = require("mongoose-findorcreate");
const ejs = require("ejs");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config({
  path: "C:/Users/user/Desktop/NCSRT/.env",
});

const app = express();

const port = 5000;
const uri = process.env.MONGO_URI;

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

const connection = mongoose.connection;
connection.once("open", () => {
  console.log("Connected to DB!");
});

app.set("view engine", "ejs");
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const usersRouter = require("./routes/user.routes");
const mentorsRouter = require("./routes/mentor.routes");
const coursesRouter = require("./routes/course.routes");
const lecturesRouter = require("./routes/lecture.routes");
//parsing json

// use morgan to log requests to the console
app.use(morgan("dev"));

//routes
app.use("/users", usersRouter);
app.use("/mentors", mentorsRouter);
app.use("/courses", coursesRouter);
app.use("/lectures", lecturesRouter);

app.listen(port, () => {
  console.log(`Server listening on port: ${port}`);
});
