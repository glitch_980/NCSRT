import { React, useState } from "react";
import axios from "axios";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    color: "#000",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: "#000",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#000",
    color: "#fff",
  },
  field: {
    borderColor: "#000",
  },
}));

export default function Register() {
  const googleRef = "http://localhost:5000/users/auth/google/PalestinianEstate";
  const facebookRef =
    "https://localhost:5000/users/auth/facebook/PalestinianEstate/?ssl=sa8s8asgahs";
  const [state, setState] = useState({
    f_name: "",
    l_name: "",
    username: "",
    phone: "",
    password: "",
    address: "",
    gender: "",
  });

  const classes = useStyles();

  function handleChange(event) {
    const { name, value } = event.target;
    setState((prev) => {
      if (name === "f_name") {
        return {
          f_name: value,
          l_name: prev.l_name,
          username: prev.username,
          phone: prev.phone,
          password: prev.password,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "l_name") {
        return {
          f_name: prev.f_name,
          l_name: value,
          username: prev.username,
          phone: prev.phone,
          password: prev.password,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "username") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: value,
          phone: prev.phone,
          password: prev.password,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "phone") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: prev.username,
          phone: value,
          password: prev.password,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "password") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: prev.username,
          phone: prev.phone,
          password: value,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "address") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: prev.username,
          phone: prev.phone,
          password: prev.password,
          address: value,
          gender: prev.gender,
        };
      } else if (name === "gender") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: prev.username,
          phone: prev.phone,
          password: prev.password,
          address: prev.address,
          gender: value,
        };
      }
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    const user = {
      f_name: state.f_name,
      l_name: state.l_name,
      username: state.username,
      phone: state.phone,
      password: state.password,
      address: state.address,
      gender: state.gender,
    };
    axios
      .post("http://localhost:5000/users/register", user)
      .then((res) => {
        window.location = "/";
        console.log("User ", user + "is added in DB!");
        res.status(200);
      })
      .catch((err) => console.error("Error logging in!", err));
  }

  return (
    <div>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon color="white" />
          </Avatar>
          <Typography component="h1" variant="h5">
            New Account
          </Typography>

          <form onSubmit={handleSubmit} className={classes.form}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  className={classes.field}
                  autoComplete="f_name"
                  name="f_name"
                  value={state.f_name}
                  onChange={handleChange}
                  variant="outlined"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  value={state.l_name}
                  onChange={handleChange}
                  id="lastName"
                  label="Last Name"
                  name="l_name"
                  autoComplete="l_name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  value={state.username}
                  onChange={handleChange}
                  id="email"
                  label="Email Address"
                  name="username"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  value={state.password}
                  onChange={handleChange}
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  value={state.address}
                  onChange={handleChange}
                  name="address"
                  label="Address"
                  type="text"
                  id="address"
                  autoComplete="Address"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  value={state.phone}
                  onChange={handleChange}
                  name="phone"
                  label="Phone"
                  id="phone"
                  autoComplete="phone"
                />
              </Grid>
              <Grid item xs={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Gender</FormLabel>
                  <RadioGroup
                    aria-label="gender"
                    name="gender"
                    value={state.gender}
                    onChange={handleChange}
                    row
                  >
                    <FormControlLabel
                      value="Female"
                      control={<Radio style={{ width: "auto" }} color="#000" />}
                      label="Female"
                    />
                    <FormControlLabel
                      value="Male"
                      control={<Radio style={{ width: "auto" }} color="#000" />}
                      label="Male"
                    />
                  </RadioGroup>
                </FormControl>
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className={classes.submit}
            >
              Sign Up
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Button
                  type="submit"
                  fullWidth
                  variant="outlined"
                  color="secondary"
                  href={googleRef}
                >
                  <img
                    src="https://img.icons8.com/plasticine/30/000000/google-logo.png"
                    alt="google-icon"
                  />
                  Sign Up with Google
                </Button>
                <Button
                  type="submit"
                  fullWidth
                  variant="outlined"
                  color="primary"
                  href={facebookRef}
                >
                  <img
                    src="https://img.icons8.com/cute-clipart/30/000000/facebook-new.png"
                    alt="facebook-icon"
                  />
                  Sign Up with Facebook
                </Button>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={5}></Box>
      </Container>
    </div>
  );
}
