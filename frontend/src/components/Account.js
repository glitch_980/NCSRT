// import React from "react";
// import PropTypes from "prop-types";
// import SwipeableViews from "react-swipeable-views";

// import Login from "./Login";
// import Register from "./Register";

// function TabPanel(props) {
//   const { children, value, index, ...other } = props;

//   return (
//     <div
//       role="tabpanel"
//       hidden={value !== index}
//       id={`full-width-tabpanel-${index}`}
//       aria-labelledby={`full-width-tab-${index}`}
//       {...other}
//     >
//       {value === index && (
//         <Box p={3}>
//           <Typography>{children}</Typography>
//         </Box>
//       )}
//     </div>
//   );
// }

// TabPanel.propTypes = {
//   children: PropTypes.node,
//   index: PropTypes.any.isRequired,
//   value: PropTypes.any.isRequired,
// };

// function a11yProps(index) {
//   return {
//     id: `full-width-tab-${index}`,
//     "aria-controls": `full-width-tabpanel-${index}`,
//   };
// }

// const useStyles = makeStyles((theme) => ({
//   root: {
//     backgroundColor: "#000",
//     width: 500,
//     alignItems: "center",
//     display: "flex",
//     flexDirection: "column",
//   },
//   paper: {
//     display: "flex",
//     flexDirection: "column",
//     alignItems: "center",
//     backgroundColor: "#fff",
//   },
//   avatar: {
//     margin: theme.spacing(1),
//     backgroundColor: "#000",
//   },
//   form: {
//     width: "100%", // Fix IE 11 issue.
//     marginTop: theme.spacing(1),
//   },
//   submit: {
//     margin: theme.spacing(3, 0, 2),
//   },
// }));

// export default function FullWidthTabs() {
//   const theme = useTheme();
//   const [value, setValue] = React.useState(0);

//   const handleChange = (event, newValue) => {
//     setValue(newValue);
//   };

//   const handleChangeIndex = (index) => {
//     setValue(index);
//   };

//   const classes = useStyles();
//   return (
//     <div className={classes.paper}>
//       <AppBar color="default">
//         <Tabs
//           value={value}
//           onChange={handleChange}
//           indicatorColor="#000"
//           textColor="#000"
//           variant="fullWidth"
//           aria-label="full width tabs example"
//         >
//           <Tab label="Login " {...a11yProps(0)} />
//           <Tab label="New Account" {...a11yProps(1)} />
//         </Tabs>
//       </AppBar>
//       <SwipeableViews
//         axis={theme.direction === "rtl" ? "x-reverse" : "x"}
//         index={value}
//         onChangeIndex={handleChangeIndex}
//       >
//         <TabPanel value={value} index={0} dir={theme.direction}>
//           <Login />
//         </TabPanel>
//         <TabPanel value={value} index={1} dir={theme.direction}>
//           <Register />
//         </TabPanel>
//       </SwipeableViews>
//       <Copyright />
//     </div>
//   );
// }
