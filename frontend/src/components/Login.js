import { React, useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import { Form, Button } from "react-bootstrap";
import Container from "@material-ui/core/Container";
import axios from "axios";
import GoogleLogin from "react-google-login";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(3),
    backgroundColor: "#000",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(2),
    borderBlockColor: "#000",
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#000",
    color: "#fff",
  },
  header: {
    margin: theme.spacing(4),
  },
}));

export default function LogIn() {
  const classes = useStyles();
  const [state, setState] = useState({
    emailPhone: "",
    password: "",
  });

  function handleChange(event) {
    const { name, value } = event.target;
    setState((prev) => {
      if (name === "emailPhone") {
        return {
          emailPhone: value,
          password: prev.password,
        };
      } else if (name === "password") {
        return {
          emailPhone: prev.emailPhone,
          password: value,
        };
      }
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    const user = {
      emailPhone: state.emailPhone,
      password: state.password,
    };
    axios
      .post("http://localhost:5000/users/login", user)
      .then((res) => {
        window.location = "/Home";
        console.log("User ", user + "is logged in!");
        res.status(200);
      })
      .catch((err) => console.error("Error logging in!", err));
  }

  return (
    <div>
      <div className={classes.header}>
        <img
          src="https://img.icons8.com/metro/26/000000/lock-2.png"
          alt="lock"
        />

        <h3>Log in</h3>
      </div>
      <Form className={classes.form} onSubmit={handleSubmit}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            onChange={handleChange}
            name="emailPhone"
            type="email"
            placeholder="Enter email"
            value={state.value}
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            name="password"
            onChange={handleChange}
            value={state.password}
            type="password"
            placeholder="Password"
          />
        </Form.Group>
        <Form.Group controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Check me out" />
        </Form.Group>
        <Button variant="dark btn btn-lg" type="submit">
          Login
        </Button>
      </Form>
    </div>
  );
}
