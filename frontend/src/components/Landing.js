import React from "react";
import AppBar from "./AppBar";
import Head from "./Head";

export default function Landing() {
  return (
    <div>
      <AppBar />
      <Head />
    </div>
  );
}
