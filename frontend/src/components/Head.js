import React from "react";
import { Carousel } from "react-bootstrap";
import pic1 from "../Media/Carousel/carousel1.jpg";
import pic2 from "../Media/Carousel/carousel2.jpg";
import pic3 from "../Media/Carousel/carousel3.jpg";
import "../css/hero.css";

export default function Head() {
  return (
    <Carousel className="caro">
      <Carousel.Item interval={5000}>
        <img className="d-block w-100" src={pic1} alt="First slide" />
        <Carousel.Caption>
          <h3>Your bright future is our mission</h3>
          <p>Any career has a key, and we have that key.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={5000}>
        <img className="d-block w-100" src={pic2} alt="Third slide" />
        <Carousel.Caption>
          <h3>It's never too late</h3>
          <p>You can apply anytime, because age doesn't matter.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={pic3} alt="Third slide" />
        <Carousel.Caption>
          <h3>Join us now</h3>
          <p>Unlock unlimited knowledge &amp; skills for the best prices</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}
