import React from "react";

import { Navbar, Form, Nav, Button } from "react-bootstrap";

export default function BackToTop(props) {
  return (
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand href="#home">NCSRT</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Link href="#home">Home</Nav.Link>
        <Nav.Link href="#about">About</Nav.Link>
        <Nav.Link href="#pricing">Courses</Nav.Link>
        <Nav.Link href="#contact">Contact</Nav.Link>
      </Nav>
      <Form inline>
        <Button href="http://localhost:3000/account" variant="outline-light">
          Join Us
        </Button>
      </Form>
    </Navbar>
  );
}
