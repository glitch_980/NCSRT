import { React, useState } from "react";
import {
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  HelpBlock,
  ButtonToolbar,
  Button,
} from "rsuite";
import axios from "axios";

export default function New() {
  const [state, setState] = useState({
    f_name: "",
    l_name: "",
    username: "",
    phone: "",
    password: "",
    address: "",
    gender: "",
  });
  function handleChange(event) {
    const { name, value } = event.target;
    setState((prev) => {
      if (name === "f_name") {
        return {
          f_name: value,
          l_name: prev.l_name,
          username: prev.username,
          phone: prev.phone,
          password: prev.password,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "l_name") {
        return {
          f_name: prev.f_name,
          l_name: value,
          username: prev.username,
          phone: prev.phone,
          password: prev.password,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "username") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: value,
          phone: prev.phone,
          password: prev.password,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "phone") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: prev.username,
          phone: value,
          password: prev.password,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "password") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: prev.username,
          phone: prev.phone,
          password: value,
          address: prev.address,
          gender: prev.gender,
        };
      } else if (name === "address") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: prev.username,
          phone: prev.phone,
          password: prev.password,
          address: value,
          gender: prev.gender,
        };
      } else if (name === "gender") {
        return {
          f_name: prev.f_name,
          l_name: prev.l_name,
          username: prev.username,
          phone: prev.phone,
          password: prev.password,
          address: prev.address,
          gender: value,
        };
      }
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    const user = {
      f_name: state.f_name,
      l_name: state.l_name,
      username: state.username,
      phone: state.phone,
      password: state.password,
      address: state.address,
      gender: state.gender,
    };
    axios
      .post("http://localhost:5000/users/register", user)
      .then((res) => {
        window.location = "/";
        console.log("User ", user + "is added in DB!");
        res.status(200);
      })
      .catch((err) => console.error("Error logging in!", err));
  }
  return (
    <Form onSubmit={handleSubmit} layout="horizontal">
      <FormGroup>
        <ControlLabel>First Name</ControlLabel>
        <FormControl
          name="f_name"
          value={state.f_name}
          onChange={handleChange}
        />
        <HelpBlock>Required</HelpBlock>
        <ControlLabel>Last Name</ControlLabel>
        <FormControl
          name="l_name"
          value={state.l_name}
          onChange={handleChange}
        />
        <HelpBlock>Required</HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>Email</ControlLabel>
        <FormControl
          name="username"
          value={state.username}
          onChange={handleChange}
          type="email"
        />
        <HelpBlock tooltip>Required</HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>Password</ControlLabel>
        <FormControl
          value={state.password}
          onChange={handleChange}
          name="password"
          type="password"
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Textarea</ControlLabel>
        <FormControl name="textarea" rows={5} componentClass="textarea" />
      </FormGroup>
      <FormGroup>
        <ButtonToolbar>
          <Button appearance="primary">Submit</Button>
          <Button appearance="default">Cancel</Button>
        </ButtonToolbar>
      </FormGroup>
    </Form>
  );
}
