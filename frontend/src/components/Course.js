import React from "react";
import { Card, Button } from "react-bootstrap";

export default function Course(props) {
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img variant="top" src="./Media" />
      <Card.Body>
        <Card.Title>{props.title}</Card.Title>
        <Card.Text>{props.description}</Card.Text>
        <Card.Text>Price: {props.price}</Card.Text>
        <Button variant="dark">Enroll now</Button>
      </Card.Body>
    </Card>
  );
}
