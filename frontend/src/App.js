import "./App.css";
import "semantic-ui-css/semantic.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Admin from "./components/AdminMenu";
import Landing from "./components/Landing";
import Account from "./components/Account";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <Router>
        <Route path="/" exact component={Landing} />
        <Route path="/account" component={Account} />
        <Route path="/admin" component={Admin}></Route>
      </Router>
    </div>
  );
}

export default App;
