const router = require("express").Router();
const usersControl = require("../controllers/users.controller");

router.post("/register", usersControl.addUser);
router.post("/login", usersControl.login);
router.get("/", usersControl.getAllUsers);
router.get("/:id", usersControl.getUser);
router.patch("/updateContact/:id", usersControl.changeContactInfo);
router.patch("/changePassword/:id", usersControl.changePassword);
router.delete("/delete/:id", usersControl.deleteAccount);
module.exports = router;
