const router = require("express").Router();
const mentorsController = require("../controllers/mentors.controller");

router.get("/", mentorsController.getAllMentors);
router.get("/:id", mentorsController.getMentor);
router.post("/add", mentorsController.addMentor);
router.post("/login", mentorsController.login);
router.patch("/changeWage/:id", mentorsController.updateWage);
router.patch("/changePass/:id", mentorsController.changePassword);
router.patch("/changeInfo/:id", mentorsController.changeContactInfo);
router.delete("/delete/:id", mentorsController.deleteAccount);

module.exports = router;
