const router = require("express").Router();
const courseControl = require("../controllers/courses.controllers");

router.post("/new", courseControl.addCourse);

router.get("/:id", courseControl.getCourse);
router.get("/", courseControl.getAllCourses);
router.get("/students/:id", courseControl.getAllStudents);
router.get("/mentors/:id", courseControl.getAllMentors);
router.get("/course/lectures/:id", courseControl.getCourseLectures);
router.patch("/assignMentor/:id", courseControl.assignMentor);
router.patch("/register/:id", courseControl.registerStudent);
router.patch("/update/:id", courseControl.updateCourse);
router.delete("/delete/:id", courseControl.deleteCourse);

module.exports = router;
