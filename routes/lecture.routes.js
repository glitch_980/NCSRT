const router = require("express").Router();
const lecturesControl = require("../controllers/lectures.controller");

router.post("/new/:id", lecturesControl.addLecture);

router.get("/", lecturesControl.getAllLectures);
router.get("/:id", lecturesControl.getLecture);

router.patch("/update/:id", lecturesControl.updateLecture);
router.delete("/delete/:id", lecturesControl.deleteLecture);

module.exports = router;
