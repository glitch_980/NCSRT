const User = require("../models/users.model");
const bcrypt = require("bcryptjs");
const saltRounds = 10;
const passport = require("passport");
const jwt = require("jsonwebtoken");

const dotenv = require("dotenv").config();

const secretKey = process.env.JWT_SECRET;

module.exports = {
  addUser: async (req, res) => {
    bcrypt.hash(req.body.password, saltRounds, async (err, hash) => {
      const newUser = new User({
        f_name: req.body.fname,
        l_name: req.body.lname,
        phone: req.body.phone,
        username: req.body.username,
        password: hash,
        address: req.body.address,
        gender: req.body.gender,
      });
      await newUser
        .save()
        .then(() => res.json("User Added"))
        .catch((error) => res.status(400).send(error));
    });
  },

  login: async (req, res) => {
    var loginType;
    if (req.body.emailPhone != "" && req.body.password != "") {
      if (isNaN(req.body.emailPhone)) loginType = "username";
      else loginType = "phoneNo";
      User.findOne()
        .where(loginType, req.body.emailPhone)
        .exec((err, data) => {
          if (err) res.status(400).send(err);
          //check if user exists
          else if (data) {
            bcrypt.compare(
              //check if password correct
              req.body.password,
              data.password,
              function (error, passMatch) {
                if (error) res.status(400).send(error);
                else if (passMatch) {
                  let jwtData = {
                    _id: data["_id"],
                    f_name: data["f_name"],
                    l_name: data["l_name"],
                    username: data["username"],
                    isAdmin: data["isAdmin"],
                  };
                  var token = jwt.sign({ user: jwtData }, secretKey);
                  res
                    .status(200)
                    .json({ message: "Login Successful", token: token });
                } else {
                  res.status(401).json({ message: "Invalid Credentials1" });
                }
              }
            );
          } else res.status(401).json({ message: "Invalid Credentials2" });
        });
    } else res.status(400).json({ message: "Provide all Credentials" });
  },
  //Todo: Write logout function when you get to the authentication

  changePassword: async (req, res) => {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const newPassConf = req.body.newPassConf;
    const id = req.params.id;

    const user = await User.findOne({ _id: id });
    bcrypt.hash(newPassword, saltRounds, async (error, hash) => {
      bcrypt.compare(oldPassword, user.password, async (err, result) => {
        if (result === true && newPassword === newPassConf) {
          const newUser = await user.updateOne({ password: hash });
          res.status(200).send(newUser);
        } else {
          res.status(400).send(err || error);
        }
      });
    });
  },

  getUser: async (req, res) => {
    const id = req.params.id;
    const user = await User.findOne({ _id: id });
    if (user) {
      res.status(200).send(user);
    } else {
      res.status(400).send("Wrong Credentials");
    }
  },

  changeContactInfo: async (req, res) => {
    const newPhone = req.body.newPhone;
    const newEmail = req.body.newEmail;
    const id = req.params.id;

    await User.findOneAndUpdate(
      { _id: id },
      { $set: { phone: newPhone, email: newEmail } },
      { new: true },
      (err, user) => {
        if (err) {
          console.log("Something wrong when updating data!");
          res.status(400).send("Error Updating User");
        }
        res.status(200).send(user);
      }
    );
  },
  deleteAccount: async (req, res) => {
    const id = req.params.id;
    const password = req.body.password;
    const user = await User.findOne({ _id: id });
    bcrypt.compare(password, user.password, async (err, result) => {
      if (result === true) {
        await User.deleteOne({ _id: id });
        res.status(200).send("User deleted from DB!");
      } else {
        res.status(400).send(err);
      }
    });
  },
  getAllUsers: async (req, res) => {
    try {
      let users = await User.find({});
      res.json(users);
    } catch (error) {
      res.status(400).send(err);
    }
  },
};
