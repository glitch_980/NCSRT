const Mentor = require("../models/mentors.model");
const bcrypt = require("bcryptjs");

const saltRounds = 10;
module.exports = {
  addMentor: async (req, res) => {
    bcrypt.hash(req.body.password, saltRounds, async (err, hash) => {
      const newMentor = new Mentor({
        f_name: req.body.fname,
        l_name: req.body.lname,
        phone: req.body.phone,
        email: req.body.email,
        password: hash,
        address: req.body.address,
        gender: req.body.gender,
      });
      await newMentor
        .save()
        .then(() => res.json("Mentor Added"))
        .catch((error) => res.status(400).send(error));
    });
  },

  login: async (req, res) => {
    var loginType;
    if (req.body.emailPhone != "" && req.body.password != "") {
      if (isNaN(req.body.emailPhone)) loginType = "username";
      else loginType = "phoneNo";
      Mentor.findOne()
        .where(loginType, req.body.emailPhone)
        .exec((err, data) => {
          if (err) res.status(400).send(err);
          //check if user exists
          else if (data) {
            bcrypt.compare(
              //check if password correct
              req.body.password,
              data.password,
              function (error, passMatch) {
                if (error) res.status(400).send(error);
                else if (passMatch) {
                  let jwtData = {
                    _id: data["_id"],
                    fname: data["fname"],
                    lname: data["lname"],
                    username: data["username"],
                    isAdmin: data["isAdmin"],
                  };
                  var token = jwt.sign({ user: jwtData }, secretKey);
                  res
                    .status(200)
                    .json({ message: "Login Successful", token: token });
                } else {
                  res.status(401).json({ message: "Invalid Credentials1" });
                }
              }
            );
          } else res.status(401).json({ message: "Invalid Credentials2" });
        });
    } else res.status(400).json({ message: "Provide all Credentials" });
  },
  //Todo: Write logout function when you get to the authentication

  changePassword: async (req, res) => {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const newPassConf = req.body.newPassConf;
    const id = req.params.id;

    const mentor = await Mentor.findOne({ _id: id });
    bcrypt.hash(newPassword, saltRounds, async (error, hash) => {
      bcrypt.compare(oldPassword, mentor.password, async (err, result) => {
        if (result === true && newPassword === newPassConf) {
          const newMentor = await mentor.updateOne({ password: hash });
          res.status(200).send(newMentor);
        } else {
          res.status(400).send(err || error);
        }
      });
    });
  },

  getMentor: async (req, res) => {
    const id = req.params.id;
    const mentor = await Mentor.findOne({ _id: id });
    if (mentor) {
      res.status(200).send(mentor);
    } else {
      res.status(400).send("Wrong Credentials");
    }
  },

  changeContactInfo: async (req, res) => {
    const newPhone = req.body.newPhone;
    const newEmail = req.body.newEmail;
    const id = req.params.id;

    await Mentor.findOneAndUpdate(
      { _id: id },
      { $set: { phone: newPhone, email: newEmail } },
      { new: true },
      (err, mentor) => {
        if (err) {
          console.log("Something wrong when updating data!");
          res.status(400).send("Error Updating Mentor");
        }
        res.status(200).send(mentor);
      }
    );
  },
  deleteAccount: async (req, res) => {
    const id = req.params.id;
    const password = req.body.password;
    const mentor = await Mentor.findOne({ _id: id });
    bcrypt.compare(password, mentor.password, async (err, result) => {
      if (result === true) {
        await Mentor.deleteOne({ _id: id });
        res.status(200).send("Mentor deleted from DB!");
      } else {
        res.status(400).send(err);
      }
    });
  },
  getAllMentors: async (req, res) => {
    try {
      let mentors = await Mentor.find({});
      res.json(mentors);
    } catch (error) {
      res.status(400).send(err);
    }
  },
  updateWage: async (req, res) => {
    const wage = req.body.wage;
    const id = req.params.id;
    const mentor = await Mentor.findOne({ _id: id });
    if (mentor != null) {
      await mentor.updateOne({
        wage: wage,
      });
      res.status(200).send(mentor);
    } else {
      res.status(400).send("Error updating info");
    }
  },
};
