const Lecture = require("../models/lectures.model");
const Course = require("../models/courses.model");

module.exports = {
  addLecture: async (req, res) => {
    const course_id = req.params.id;
    await Course.findOne({ _id: course_id }, (error, course) => {
      if (error) {
        res.status(400).send(error);
      } else {
        const newLecture = new Lecture({
          title: req.body.title,
          week: req.body.week,
          duration: req.body.duration,
          link: req.body.link,
          date: req.body.date,
          course: course_id,
          time: req.body.time,
        });
        newLecture
          .save()
          .then(() => res.json(newLecture))
          .catch((err) => res.status(400).send(err));
      }
    });
  },
  getAllLectures: async (req, res) => {
    await Lecture.find({}, (err, lectures) => {
      if (err) {
        res.status(400).send(err);
      } else {
        res.json(lectures);
      }
    });
  },
  updateLecture: async (req, res) => {
    const id = req.params.id;
    const newTitle = req.body.title;
    const newLink = req.body.link;
    const newTime = req.body.time;
    const newDate = req.body.date;
    const newWeek = req.body.week;
    const newDur = req.body.duration;
    const newLecture = new Lecture({
      _id: id,
      title: newTitle,
      duration: newDur,
      link: newLink,
      date: newDate,
      time: newTime,
      week: newWeek,
    });

    await Lecture.updateOne({ _id: id }, newLecture)
      .then(() => {
        res.status(201).send(newLecture);
      })
      .catch((error) => {
        res.status(400).send(error);
      });
  },

  deleteLecture: async (req, res) => {
    const id = req.params.id;
    try {
      await Lecture.findByIdAndDelete(id);
      const lectures = await Lecture.find({});
      res.status(200).send(lectures);
    } catch (error) {
      res.status(400).send(error);
    }
  },
  getLecture: async (req, res) => {
    const id = req.params.id;
    await Lecture.findOne({ _id: id }, (err, lecture) => {
      if (err) {
        res.status(400).send(err);
      } else {
        res.json(lecture);
      }
    });
  },

  //TODO: GET ALL STUDENTS IN LECTURE
  //TODO: GET MENTOR OF LECTURE
};
