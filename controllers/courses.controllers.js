const Mentor = require("../models/mentors.model");
const Student = require("../models/users.model");
const Course = require("../models/courses.model");
const Lecture = require("../models/lectures.model");

module.exports = {
  addCourse: async (req, res) => {
    const newCourse = new Course({
      title: req.body.title,
      hours: req.body.hours,
      duration: req.body.duration,
      meetings: req.body.meetings,
      m_length: req.body.m_length,
      price: req.body.price,
    });
    await newCourse
      .save()
      .then(() => res.json(newCourse))
      .catch((err) => res.status(400).send(err));
  },
  getCourse: async (req, res) => {
    const id = req.params.id;
    await Course.findOne({ _id: id }, (err, course) => {
      if (err) {
        res.status(400).send(err);
      } else {
        res.json(course);
      }
    });
  },

  getAllCourses: async (req, res) => {
    await Course.find({}, (err, courses) => {
      if (err) {
        res.status(400).send(err);
      } else {
        res.json(courses);
      }
    });
  },

  deleteCourse: async (req, res) => {
    const id = req.params.id;
    try {
      await Course.findByIdAndDelete(id);
      const courses = await Course.find({});
      res.status(200).send(courses);
    } catch (error) {
      res.status(400).send(error);
    }
  },
  updateCourse: async (req, res) => {
    const id = req.params.id;
    const newTitle = req.body.title;
    const newMeetings = req.body.meetings;
    const newPrice = req.body.price;
    const newHours = req.body.hours;
    const newML = req.body.m_length;
    const newDur = req.body.duration;
    const newCourse = new Course({
      _id: id,
      title: newTitle,
      duration: newDur,
      hours: newHours,
      m_length: newML,
      meetings: newMeetings,
      price: newPrice,
    });

    await Course.updateOne({ _id: id }, newCourse)
      .then(() => {
        res.status(201).send(newCourse);
      })
      .catch((error) => {
        res.status(400).send(error);
      });
  },

  assignMentor: (req, res) => {
    //TODO: write a function to add a mentor from Mentors schema to the array of mentors in courses schema
    const mentor_fname = req.body.f_name;
    const mentor_lname = req.body.l_name;
    const course_id = req.params.id;

    Mentor.findOne(
      { f_name: mentor_fname, l_name: mentor_lname },
      (err, mentor) => {
        if (err || !mentor) {
          res.status(400).send(err);
        } else {
          const id = mentor._id;
          Course.findOneAndUpdate(
            { _id: course_id },
            { $push: { mentor_s: id } },
            { new: true },
            (error, course) => {
              if (error) {
                res.status(400).send(error);
              } else {
                res.status(200).send(course);
              }
            }
          );
        }
      }
    );
  },

  registerStudent: (req, res) => {
    //TODO: write a function to enable the student to register in a course
    const title = req.body.title;
    const id = req.params.id; //Students ID

    Student.findOne({ _id: id }, (error, student) => {
      if (error) {
        res.status(400).send(error);
      } else {
        Course.findOneAndUpdate(
          { title: title },
          { $push: { students: id } },
          { new: true },
          (err, course) => {
            if (err || !course) {
              res.status(400).send(err);
            } else {
              res.status(200).send(course);
            }
          }
        );
      }
    });
  },

  getAllStudents: async (req, res) => {
    const id = req.params.id; //Course ID

    await Course.find({ _id: id })
      .populate({ path: "students", select: ["f_name", "l_name"] })
      .exec(function (err, students) {
        if (err) {
          res.status(400).send(err);
        } else {
          res.status(200).send(students);
        }
      });
  },
  getAllMentors: async (req, res) => {
    const id = req.params.id; //Course ID

    await Course.find({ _id: id })
      .populate({ path: "mentor_s", select: ["f_name", "l_name"] })
      .exec(function (err, mentors) {
        if (err) {
          res.status(400).send(err);
        } else {
          res.status(200).send(mentors);
        }
      });
  },
  getCourseLectures: (req, res) => {
    const id = req.params.id; //this is the course id embedded in the lecture schema
    Course.findOne({ _id: id }, (err, course) => {
      if (err) {
        res.status(400).send(err);
      } else {
        Lecture.find({ course: id }, (error, lectures) => {
          if (error) {
            res.status(400).send(error);
          } else {
            res.json(lectures);
          }
        });
      }
    });
  },
};
